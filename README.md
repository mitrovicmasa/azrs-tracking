# azrs-tracking

Projekat iz predmeta `Alati za razvoj softvera` na Matematičkom fakultetu u Beogradu.

Primena alata na projektima [Life of Pi](https://gitlab.com/mitrovicmasa/computer-graphics-project) i [Traffic Express](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2022-2023/01-traffic-express).

Izveštaji su u sekciji [Issues](https://gitlab.com/mitrovicmasa/azrs-tracking/-/issues).
